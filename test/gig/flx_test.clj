(ns gig.flx-test
  (:require [clojure.test :refer :all]
            [gig.flx :refer :all]))

(deftest scramble?-test
  (testing "Testing scrambling function"
    (is (true? (scramble? "rekqodlw" "world")))
    (is (true? (scramble? "cedewaraaossoqqyt" "codewars")))
    (is (false? (scramble? "katas"  "steak")))
    (is (false? (scramble? "uni" "unni")))))
