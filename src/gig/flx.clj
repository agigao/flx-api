(ns gig.flx
  (:require [ring.middleware.reload :refer [wrap-reload]]
            [ring.adapter.jetty :as ring]
            [ring.util.response :refer [response]]
            [compojure.core :refer [defroutes GET]]
            [compojure.route :refer [not-found]]
            [ring.handler.dump :refer [handle-dump]]
            [ring.middleware.cors :refer [wrap-cors]]))

(defn scramble? [s1 s2]
  " Check if s1 can be composed of characters from s1 "
  (let [[s1 s2] (map (comp frequencies sort) [s1 s2])]
    (every? (fn [[k v]] (<= v (get s1 k -1))) s2)))

(defroutes routes
  (GET "/" [] (response "Home of Scrambling"))
  (GET "/:s1/:s2" [s1 s2] (response (str (scramble? s1 s2))))
  (GET "/request" [req] handle-dump)
  (not-found "Page not found"))

(def app
  (wrap-cors routes
             :access-control-allow-origin [#".*"]
             :access-control-allow-methods [:get]))

(defn -main [& args]
  (ring/run-jetty app {:port (or (read-string (first args)) 8080)}))

(defn dev-main [{port :port} & args]
  (ring/run-jetty (wrap-reload #'app) {:port port :join? false}))
